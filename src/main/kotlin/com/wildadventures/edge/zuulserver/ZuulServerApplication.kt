package com.wildadventures.edge.zuulserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableZuulProxy
@EnableFeignClients
class ZuulServerApplication

fun main(args: Array<String>) {
	runApplication<ZuulServerApplication>(*args)
}

