package com.wildadventures.edge.zuulserver.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.wildadventures.edge.zuulserver.domain.User;

@Repository
@FeignClient("microservice-users")
@RequestMapping("${microservice-users.context-path}")
public interface UserClient {

	@GetMapping(path = "users/mail/{mail}")
	User getUserByMail(@RequestParam("mail") String mail);
}
