package com.wildadventures.edge.zuulserver.configuration;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "cors")
public class CorsPropertiesHolder {
	
	private boolean allowCredentials;
	private List<String> allowedHeaders;
	private List<String> allowedMethods;
	private List<String> allowedOrigins;
	private List<String> exposedHeaders;
	private long maxAge;

}
