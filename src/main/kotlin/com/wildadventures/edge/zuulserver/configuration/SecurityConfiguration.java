package com.wildadventures.edge.zuulserver.configuration;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableConfigurationProperties(CorsPropertiesHolder.class)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private CorsPropertiesHolder corsPropertiesHolder;
	
	 @Autowired
    private JWTConfig jwtConfig;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.cors()
			.and()
			.csrf().disable()
			.sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.exceptionHandling()
			.authenticationEntryPoint((request, response, exception) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED))
			.and()
			.addFilterAfter(new JWTAuthenticationFilter(jwtConfig), UsernamePasswordAuthenticationFilter.class)
			.authorizeRequests()
			.antMatchers(HttpMethod.POST, jwtConfig.getURI()).permitAll()
			.anyRequest().permitAll();

	}

	@Bean
    public JWTConfig jwtConfig() {
        return new JWTConfig();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		logger.debug("Configuring cors...");
		logger.debug("allowCredentials: " + this.corsPropertiesHolder.isAllowCredentials());
		logger.debug("allowedHeaders: " + this.corsPropertiesHolder.getAllowedHeaders());
		logger.debug("allowedMethods: " + this.corsPropertiesHolder.getAllowedMethods());
		logger.debug("allowedOrigins: " + this.corsPropertiesHolder.getAllowedOrigins());
		logger.debug("exposedHeaders: " + this.corsPropertiesHolder.getExposedHeaders());
		logger.debug("maxAge: " + this.corsPropertiesHolder.getMaxAge());
		
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowCredentials(this.corsPropertiesHolder.isAllowCredentials());
		configuration.setAllowedHeaders(this.corsPropertiesHolder.getAllowedHeaders());
		configuration.setAllowedMethods(this.corsPropertiesHolder.getAllowedMethods());
		configuration.setAllowedOrigins(this.corsPropertiesHolder.getAllowedOrigins());
		configuration.setExposedHeaders(this.corsPropertiesHolder.getExposedHeaders());
		configuration.setMaxAge(this.corsPropertiesHolder.getMaxAge());
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

}
