package com.wildadventures.edge.zuulserver.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * This is the UserDetailsG model
 * @author amarjane
 * @version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class User {

    private String mail;
    private String password;
    private String profil;
}
