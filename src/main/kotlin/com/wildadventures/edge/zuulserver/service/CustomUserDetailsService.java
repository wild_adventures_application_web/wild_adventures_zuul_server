package com.wildadventures.edge.zuulserver.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.wildadventures.edge.zuulserver.client.UserClient;
import com.wildadventures.edge.zuulserver.domain.User;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserClient userClient;
	
	@Override
	public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {

		User user = userClient.getUserByMail(mail);
		
		return org.springframework.security.core.userdetails.User.withDefaultPasswordEncoder()
					.username(user.getMail())
					.password(user.getPassword())
					.authorities(user.getProfil())
					.build();
	}

}
